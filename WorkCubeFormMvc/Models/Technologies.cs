﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WorkCubeFormMvc.Models
{
    [Table("Technologies")]
    public class Technologies
    {
        [Key,DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        [StringLength(50)]
        public string technologyName { get; set; }
        [StringLength(50)]
        public string type { get; set; }
    }
}