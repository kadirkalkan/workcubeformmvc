﻿using System;
using System.Collections.Generic;
using System.Data.Entity; // DBContext Sınıfından Kalıtım Almak İçin Ekledik.
using System.Linq;
using System.Web;

namespace WorkCubeFormMvc.Models.Managers
{
    public class DatabaseContext : DbContext // Veritabanı işlemlerini yapacak sınıf olduğunu belirtmek için DBContext'ten kalıtım aldık.
    {
        public DbSet<Testers> Testers { get; set; }
        public DbSet<Technologies> Technologies { get; set; }
        public DbSet<Questions> Questions { get; set; }
        public DbSet<Answers> Answers { get; set; }
        public DbSet<Scores> Scores { get; set; }

        public DatabaseContext()
        {
            Database.SetInitializer(new DatabaseCreator());
        }
    }

    public class DatabaseCreator : CreateDatabaseIfNotExists<DatabaseContext>
    {
        protected override void Seed(DatabaseContext context)
        {
            List<string> languages = new List<string>() { "HTML5", "CSS3", "Boostrap", "Metronics", "JavaScript", "Ajax", "JSON", "AngularJS", "Jquery", "SQL", "MSSQL", "MySQL", "PHP", ".NET", "CFML", "JSP", "Python", "C#", "C++", "Java", "Cordoba", "NodeJS", "Go", "Ruby" };

            foreach (string langName in languages)
            {
                Technologies newTechnology = new Technologies();
                newTechnology.type = "lang";
                newTechnology.technologyName= langName;
                context.Technologies.Add(newTechnology);
            }

            List<string> technologies = new List<string>() { "Git", "Win Server", "PHP Server", "Tomcat", "GitHub", "BitBucket", "SourceTree", "Linux Server", "IIS", "Apache", "SOLR", "NoSQL", "Mongo", "Windows IoT", "Watson IoT", "Raspberry", "Unity", "UnReal", "3DMax", "SolidWorks", "AfterEffects", "PhotoShop", "Premier", "Maya" };

            foreach (string techName in technologies)
            {
                Technologies newTechnology = new Technologies();
                newTechnology.technologyName = techName;
                newTechnology.type = "tech";
                context.Technologies.Add(newTechnology);
            }
            List<string> questions = new List<string>() { "Kullandığınız IDE’ler", "Başka ne biliyorsun, ilgilisin veya öğreniyorsun?", "Varmak istediğin yer neresi?", "Definition with One Sentence", "Definition with One Word", "İlk projeni ve yol haritanı yaz!" };

            foreach (string quest in questions)
            {
                Questions newQuestion = new Questions();
                newQuestion.question = quest;
                context.Questions.Add(newQuestion);
            }

            context.SaveChanges();

        }
    }
}