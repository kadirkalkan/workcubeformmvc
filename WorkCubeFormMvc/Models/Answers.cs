﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WorkCubeFormMvc.Models
{
    [Table("Answers")]
    public class Answers
    {
        [Key,DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        // StringLength(Max)
        public string answer { get; set; }

        public virtual Testers tester { get; set; }
        public virtual Questions question { get; set; }
            
    }
}