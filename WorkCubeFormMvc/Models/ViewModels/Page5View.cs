﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WorkCubeFormMvc.Models.ViewModels
{
    public class Page5View
    {
        public QuestionView Onesentence { get; set; }
        public QuestionView Oneword { get; set; }
    }
}