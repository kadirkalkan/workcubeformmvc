﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WorkCubeFormMvc.Models.ViewModels
{
    public class Page2View
    {
        public class ScoreVM
        {
            public int ID { get; set; }
            public string Name { get; set; }
            public int Score { get; set; }
        }
        public class TechnologiesVM
        {
            public List<ScoreVM> Languages { get; set; }
            public List<ScoreVM> Technologies { get; set; }
            public string Notes { get; set; } // for your textarea control
        }
    }
}