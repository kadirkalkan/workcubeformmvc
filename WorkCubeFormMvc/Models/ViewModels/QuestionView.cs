﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WorkCubeFormMvc.Models.ViewModels
{
    public class QuestionView
    {
        public int ID { get; set; }
        public string answer { get; set; }
    }
}