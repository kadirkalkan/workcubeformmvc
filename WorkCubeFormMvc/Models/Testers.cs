﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WorkCubeFormMvc.Models
{
    [Table("Testers")]
    public class Testers
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [StringLength(50),Required]
        public string testerName { get; set; }

        public ICollection<Scores> Scores { get; set; }
    }
}