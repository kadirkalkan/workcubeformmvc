﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WorkCubeFormMvc.Models
{
    [Table("Scores")]
    public class Scores
    {
        [Key,DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        [DefaultValue(0)]
        public int score { get; set; }

        public virtual Testers tester { get; set; }
        public virtual Technologies technology { get; set; }
    }
}