﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WorkCubeFormMvc.Models;
using WorkCubeFormMvc.Models.Managers;

namespace WorkCubeFormMvc.Controllers
{
    public class HomeController : Controller
    {
        DatabaseContext db = new DatabaseContext();
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Index(Testers user)
        {
            int sonuc = 0;
            db.Testers.Add(user);
            try
            {
                sonuc = db.SaveChanges();
                //Testers yeniUser = db.Testers.Where(x => x.ID == db.Testers.Max(y => y.ID)).FirstOrDefault();
                
                    return RedirectToAction("Page1", "Form");
            }
            catch
            {
                ViewBag.Result = "Lütfen Adınızı Giriniz";
                return View();
            }
        }
    }
}