﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WorkCubeFormMvc.Models;
using WorkCubeFormMvc.Models.Managers;
using WorkCubeFormMvc.Models.ViewModels;
using static WorkCubeFormMvc.Models.ViewModels.Page2View;

namespace WorkCubeFormMvc.Controllers
{
    public class FormController : Controller
    {
        DatabaseContext db = new DatabaseContext();
        public ActionResult Page1()
        {
            return View();
        }
        public ActionResult Page2()
        {
            IEnumerable<Technologies> technologies = db.Technologies;
            TechnologiesVM model = new TechnologiesVM
            {
                Languages = technologies.Where(x => x.type == "lang")
            .Select(x => new ScoreVM { ID = x.ID, Name = x.technologyName }).ToList(),
                Technologies = technologies.Where(x => x.type == "tech")
            .Select(x => new ScoreVM { ID = x.ID, Name = x.technologyName }).ToList(),
            };
            return View(model);
        }
        [HttpPost]
        public ActionResult Page2(TechnologiesVM model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            Answers answer = new Answers();
            answer.answer = model.Notes;
            answer.tester = db.Testers.Where(x => x.ID == db.Testers.Max(y => y.ID)).FirstOrDefault();
            answer.question = db.Questions.Where(x => x.ID == 1).FirstOrDefault();
            List<Scores> allScores = new List<Scores>();
            List<ScoreVM> allTechs = new List<ScoreVM>();
            allTechs.AddRange(model.Technologies);
            allTechs.AddRange(model.Languages);
            foreach (var item in allTechs)
            {
                Scores newScore = new Scores();
                newScore.tester = db.Testers.Where(x => x.ID == db.Testers.Max(y => y.ID)).FirstOrDefault();
                newScore.technology = db.Technologies.Where(x => x.ID == item.ID).FirstOrDefault();
                newScore.score = item.Score;
                allScores.Add(newScore);
            }
            db.Scores.AddRange(allScores);
            db.Answers.Add(answer);
            db.SaveChanges();
            return RedirectToAction("Page3", "Form");
        }
        public ActionResult Page3()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Page3(QuestionView model)
        {
            Answers answer = new Answers();
            Questions question = db.Questions.Where(x => x.ID == 2).FirstOrDefault();
            answer.question= question;
            answer.tester = db.Testers.Where(x => x.ID == db.Testers.Max(y=> y.ID)).FirstOrDefault();
            db.Answers.Add(answer);
            db.SaveChanges();
            return RedirectToAction("Page4","Form");
        }

        public ActionResult Page4()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Page4(QuestionView model)
        {
            Answers answer = new Answers();
            Questions question = db.Questions.Where(x => x.ID == 3).FirstOrDefault();
            answer.question = question;
            answer.tester = db.Testers.Where(x => x.ID == db.Testers.Max(y => y.ID)).FirstOrDefault();
            db.Answers.Add(answer);
            db.SaveChanges();
            return RedirectToAction("Page5", "Form");
        }
        public ActionResult Page5()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Page5(Page5View model)
        {
            List<Answers> answers = new List<Answers>() { new Answers(),new Answers() };
            Questions question = db.Questions.Where(x => x.ID == 4).FirstOrDefault();
            answers[0].question = question;
            answers[0].tester = db.Testers.Where(x => x.ID == db.Testers.Max(y => y.ID)).FirstOrDefault();
            answers[0].answer = model.Onesentence.answer;

            question = db.Questions.Where(x => x.ID == 5).FirstOrDefault();
            answers[1].question = question;
            answers[1].tester = answers[0].tester;
            answers[1].answer = model.Oneword.answer;

            db.Answers.AddRange(answers);
            db.SaveChanges();
            return RedirectToAction("Page6", "Form");
        }
        public ActionResult Page6()
        {
            return View();
        }
        public ActionResult Page7()
        {
          return View();
        }
        [HttpPost]
        public ActionResult Page7(QuestionView model)
        {
            Answers answer = new Answers();
            Questions question = db.Questions.Where(x => x.ID == 6).FirstOrDefault();
            answer.question = question;
            answer.tester = db.Testers.Where(x => x.ID == db.Testers.Max(y => y.ID)).FirstOrDefault();
            db.Answers.Add(answer);
            db.SaveChanges();
            return RedirectToAction("Page8", "Form");
        }
        public ActionResult Page8()
        {
            return View();
        }
        public ActionResult Page9()
        {
            return View();
        }
        public ActionResult Page10()
        {
            return View();
        }
        public ActionResult Page11()
        {
            return View();
        }
        public ActionResult Page12()
        {
            return View();
        }
    }
}